# Use the official Rust image as the build environment
FROM rust:latest as builder

WORKDIR /usr/src/mini_10

# Copy the source code and libtorch directory into the container
COPY ./src ./src
COPY ./libtorch ./libtorch
COPY Cargo.lock .
COPY Cargo.toml .

# Install necessary packages for building the application
RUN apt-get update -y && \
    apt-get install -y pkg-config cmake libssl-dev

# Set environment variables for libtorch
ENV LIBTORCH=/usr/src/mini_10/libtorch
ENV LIBTORCH_INCLUDE=/usr/src/mini_10/libtorch/include
ENV LIBTORCH_LIB=/usr/src/mini_10/libtorch/lib
ENV LD_LIBRARY_PATH=/usr/src/mini_10/libtorch/lib:$LD_LIBRARY_PATH

# Build the application
RUN cargo build --release

# Create a new lightweight container for the application
FROM debian:buster-slim

WORKDIR /usr/local/bin

# Since the torch deps are shared libs, they are not packaged in the binary
# Copy the libtorch libraries if they are needed at runtime
COPY --from=builder /usr/src/mini_10/libtorch ./libtorch

# Copy the compiled binary from the builder stage into the lightweight container
COPY --from=builder /usr/src/mini_10/target/release/mini_10 .

# Update the library path to include libtorch
ENV LD_LIBRARY_PATH=/usr/local/bin/libtorch/lib:$LD_LIBRARY_PATH

# Command to run the application
CMD ["mini_10"]
