## Create a project that uses the Hugging Face Rust transformer. 

1. Initialize a Project
2. Add Dependencies for Hugging Face Transformer
3. Install libtorch manually and set the LIBTORCH environment variable

```
curl -o libtorch-cxx11-abi-shared-with-deps-2.1.0+cpu.zip https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-2.1.0%2Bcpu.zip
unzip libtorch-cxx11-abi-shared-with-deps-2.1.0+cpu.zip -d ./libtorch

```

```
ENV LIBTORCH=./ld_lib/libtorch
ENV LIBTORCH_INCLUDE=./ld_lib/libtorch
ENV LIBTORCH_LIB=./ld_lib/libtorch
ENV LD_LIBRARY_PATH=./ld_lib/libtorch/lib:$LD_LIBRARY_PATH
```

4. Write Rust Code to Interact with Hugging Face
5. Build the Project

```
cargo build
```

## Dockerize the Hugging Face Rust Transformer

1. Create a Dockerfile
2. Build and Test the Docker Image
```
docker build -t mini_10 .
```

## Deploy the Container to AWS Lambda
1. Create an ECR Repository
2. Push the Docker Image to ECR

```
aws ecr get-login-password --region your-region | docker login --username AWS --password-stdin your-account-id.dkr.ecr.your-region.amazonaws.com
docker tag mini_10:latest your-account-id.dkr.ecr.your-region.amazonaws.com/mini_10:latest
docker push your-account-id.dkr.ecr.your-region.amazonaws.com/mini_10:latest
```

3. Create a Lambda Function with the Container Image

![Alt text](screenshot1.jpg)

## Implement the Query Endpoint
1. Set up API Gateway

![Alt text](screenshot2.jpg)

2. Test the Endpoint
```
 curl -X POST https://abx92xcs0l.execute-api.us-east-1.amazonaws.com/dev/query \   
-H "Content-Type: application/json" \
-d '{"question":"What is the capital of France?", "context":"Paris is the capital of France."}'
```


