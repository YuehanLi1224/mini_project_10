use lambda_http::{
    handler,
    lambda_runtime::{self, Context, Error},
    IntoResponse, Request, Response, Body, RequestExt
};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use serde_json::json;

#[derive(Deserialize)]
struct InputData {
    question: String,
    context: String,
}

#[derive(Serialize)]
struct OutputData {
    answer: String,
}

async fn query_huggingface_api(question: String, context: String) -> Result<String, reqwest::Error> {
    let client = Client::new();
    let response = client.post("https://api-inference.huggingface.co/models/distilbert-base-uncased")
        .header("Authorization", "Bearer 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")
        .json(&json!({"inputs": {"question": question, "context": context}}))
        .send()
        .await?
        .json::<serde_json::Value>()
        .await?;

    Ok(response["answer"].as_str().unwrap_or_default().to_string())
}

async fn function_handler(event: Request, _: Context) -> Result<impl IntoResponse, Error> {
    let data: Option<InputData> = event.payload().unwrap_or(None);

    match data {
        Some(input_data) => {
            let answer = query_huggingface_api(input_data.question, input_data.context).await?;
            let response = json!({ "answer": answer });
            Ok(Response::builder()
                .status(200)
                .header("Content-Type", "application/json")
                .body(Body::from(response.to_string()))
                .expect("Failed to render response"))
        },
        None => {
            let response = json!({ "error": "No data provided" });
            Ok(Response::builder()
                .status(400)
                .header("Content-Type", "application/json")
                .body(Body::from(response.to_string()))
                .expect("Failed to render response"))
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler(function_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
